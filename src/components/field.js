import React, { Component } from 'react';
import Colomn from './column';

let dataField = Array(0).fill(0);
class Field extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            field: Array(0).fill(null), 
            infoGame: "Green player", 
            flagPlayer: false,
            flagGame: true };
        this.addColumn = this.addColumn.bind(this);
        this.isWinner = this.isWinner.bind(this);
    }

    addColumn(column) {
        let arr = this.state.field.push(column);
        this.setState({ field: arr });
        dataField = this.state.field;
    }

    isWinner() {
        if (this.state.flagPlayer) {
            this.setState({ infoGame: "Green player" });
            this.setState({ flagPlayer: false })
        }
        else {
            this.setState({ infoGame: "Red player" });
            this.setState({ flagPlayer: true });
        }
        let winner = null, amountItem = 0;
        dataField.forEach((column) => {
            let amountGreen = 0, amountRed = 0;
            column.forEach((element) => {
                if (element === "Green") {
                    amountGreen++;
                    amountRed = 0;
                    if (amountGreen === 4) winner = "Winner is Green!";
                }
                if (element === "Red") {
                    amountRed++;
                    amountGreen = 0;
                    if (amountRed === 4) winner = "Winner is Red!";
                }
                if (element === "Item") amountItem++;
            })
        })
        if (winner) {
            this.setState({ flagGame:false, infoGame: winner });
            return winner;
        }
        else {
            for (let row = 0; row < 6; row++) {
                let amountGreen = 0, amountRed = 0;
                for (let column = 0; column < 7; column++) {
                    if (dataField[column][row] === "Red") {
                        amountRed++;
                        amountGreen = 0;
                        if (amountRed === 4) winner = "Winner is Red!";
                    }
                    if (dataField[column][row] === "Green") {
                        amountGreen++;
                        amountRed = 0;
                        if (amountGreen === 4) winner = "Winner is Green";
                    }
                }
            }
        }

        if (amountItem === 0) {
            winner = "Drawn game!";
            this.setState({ flagGame:false, infoGame: winner });
        }
        if (winner) this.setState({ flagGame:false, infoGame: winner });
        return winner;
    }

    render() { 
        return (<div>
            <div>
                {this.state.infoGame}
            </div>
            <Colomn addColumn={this.addColumn} isWinner={this.isWinner} flagGame={this.state.flagGame} id="0" />
            <Colomn addColumn={this.addColumn} isWinner={this.isWinner} flagGame={this.state.flagGame} id="1" />
            <Colomn addColumn={this.addColumn} isWinner={this.isWinner} flagGame={this.state.flagGame} id="2" />
            <Colomn addColumn={this.addColumn} isWinner={this.isWinner} flagGame={this.state.flagGame} id="3" />
            <Colomn addColumn={this.addColumn} isWinner={this.isWinner} flagGame={this.state.flagGame} id="4" />
            <Colomn addColumn={this.addColumn} isWinner={this.isWinner} flagGame={this.state.flagGame} id="5" />
            <Colomn addColumn={this.addColumn} isWinner={this.isWinner} flagGame={this.state.flagGame} id="6" />
        </div>);
    }
}

export default Field;

