import React, { Component } from 'react';
import Element from './element';
import '.././item.css';

let inc = 0;
class Coloumn extends Component {

    constructor(props) {
        super(props);
        this.arr = (Array(6).fill("Item"));
        this.state = {
            color: this.arr,
            count: 0
        };
        this.returnColor = this.returnColor.bind(this);
        this.props.addColumn(this.state.color);
    }

    returnColor() {
        let i = this.state.count;
        if (inc % 2 === 0 && i < 6 && this.props.flagGame) {
            this.arr[i] = "Green";
            this.setState({ count: i + 1, color: this.arr });
            this.props.isWinner()
            inc++;
        }
        else if (inc % 2 !== 0 && i < 6 && this.props.flagGame) {
            this.arr[i] = "Red";
            this.setState({ count: i + 1, color: this.arr });
            this.props.isWinner()
            inc++;
        }
    }

    render() {
        return (
            <div className="Columns" onClick={this.returnColor}>
                <Element color={this.state.color[5]} id="6" />
                <Element color={this.state.color[4]} id="5" />
                <Element color={this.state.color[3]} id="4" />
                <Element color={this.state.color[2]} id="3" />
                <Element color={this.state.color[1]} id="2" />
                <Element color={this.state.color[0]} id="1" />
            </div>);
    }
}

export default Coloumn;