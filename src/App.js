import React, { Component } from 'react';
import './App.css';
import Field from './components/field';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <Field/>
        </header>  
      </div>
    );
  }
}

export default App;
